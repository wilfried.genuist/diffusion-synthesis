
## 1. Diffusion models, discrete time approach <span style="color:#ff0000">DDPM</span> [[Denoising Diffusion Probabilistic Models.pdf]]:

#### <u>Unconditional approach</u>

The model's marginal distribution is assumed to be $p(\mathbf{x})=\int p(\mathbf{x}, \mathbf{z}) \mathrm{d} \mathbf{z}$ . The distribution that "best" fits the data (who’s distribution is $\left.p_{\text {data }}(\mathbf{x})\right)$ is obtained by minimizing the KL divergence:
$$
\min _{p \in \mathcal{P}_{\mathbf{x}, \mathbf{z}}} D_{\mathrm{KL}}\left(p_{\text {data }}(\mathbf{x}) \| p(\mathbf{x})\right) \text {. }
$$
Under a Markovian HVAE (Hierarchical Variational Auto-Encoder) hypothesis (successive variational auto-encoders + Markovian hypothesis) we get an $ELBO$ minimization objective:
$$
\log p(\boldsymbol{x}) =\log\int p(\boldsymbol{x}_{0:T})d\boldsymbol{x}_{1:T}\geq\mathbb{E}_{q(\boldsymbol{x}_{1:T}|\boldsymbol{x}_{0})}\left[\log\frac{p(\boldsymbol{x}_{0:T})}{q(\boldsymbol{x}_{1:T}|\boldsymbol{x}_{0})}\right]=\operatorname{ELBO}
$$
where $p$ is the <span style="color:#ff0000">learned denoising process</span> and $q$ is <span style="color:#ff0000">Gaussian noise</span>. Under that Markovian hypothesis, the objective can be re-written as ([[Understanding Diffusion Models, A Unified Perspective.pdf]]): 
$$
\begin{align}
  
\arg\min_{\boldsymbol{p_{\theta_{\text{reverse}}}}}
\underbrace{\mathbb{E}_{q(\boldsymbol{x}_1|\boldsymbol{x}_0)}\left[\log p_{\boldsymbol{\theta}}(\boldsymbol{x}_0|\boldsymbol{x}_1)\right]}_{\text{reconstruction term}}-\underbrace{D_{\mathrm{KL}}(q(\boldsymbol{x}_T|\boldsymbol{x}_0)\parallel p(\boldsymbol{x}_T))}_{\text{prior matching term}}-\\ \sum_{t=2}^T\underbrace{\mathbb{E}_{q(\boldsymbol{x}_t|\boldsymbol{x}_0)}\left[D_{\mathrm{KL}}(q(\boldsymbol{x}_{t-1}|\boldsymbol{x}_t,\boldsymbol{x}_0)\parallel p_{\boldsymbol{\theta}}(\boldsymbol{x}_{t-1}|\boldsymbol{x}_t))\right]}_{\text{denoising matching term}} \\

\end{align}


$$
The denoising matching term is the important criteria to optimize, it will guaranty a good image reconstruction after noise has been added. We start by rewriting $q$ knowing the noising schedule of states $\{x_{t}\}$ (using $\alpha_t=1-\beta_t\text{ and }\bar{\alpha}_t=\prod_{i=1}^T\alpha_i$):
$$\begin{cases}\mathbf{x}_t=\sqrt{\alpha_t}\mathbf{x}_{t-1}+\sqrt{1-\alpha_t}\boldsymbol{\epsilon}_{t-1}\text{ where }\boldsymbol{\epsilon}_{t-1}\sim\mathcal{N}(\mathbf{0},\mathbf{I})
\\ \\

q(\mathbf{x}_{t-1}|\mathbf{x}_t,\mathbf{x}_0)\propto\mathcal{N}(x_{t-1};\underbrace{\frac{\sqrt{\alpha_t}(1-\bar{\alpha}_{t-1})x_t+\sqrt{\bar{\alpha}_{t-1}}(1-\alpha_t)x_0}{1-\bar{\alpha}_t}}_{\mu_q(\boldsymbol{x}_t,\boldsymbol{x}_0)},\underbrace{\frac{(1-\alpha_t)(1-\bar{\alpha}_{t-1})}{1-\bar{\alpha}_t}\mathbf{I})}_{\boldsymbol{\Sigma}_q(t)}

&\end{cases}$$
We can rewrite our objective as finding the proper mean of $p_{\theta}\propto\mathcal{N}\left(x_{t-1};\mu_{\boldsymbol{\theta}},\Sigma_{q}\left(t\right)\right)$ to learn the denoising process (**the reverse process is assumed Gaussian**, the variance was defined to be preserved throughout the process, thus not learned but fixed to be $\Sigma_{q}$):
$$\begin{aligned}
 \underset{\theta}{\operatorname*{\arg\min}}D_{\mathrm{KL}}&(q(x_{t-1}|x_{t},x_{0})\parallel p_{\boldsymbol{\theta}}(x_{t-1}|x_{t})) 
\
&=\underset{\boldsymbol{\theta}}{\operatorname*{\arg\min}}D_{\mathrm{KL}}(\mathcal{N}\left(x_{t-1};\mu_{q},\Sigma_{q}\left(t\right)\right)\parallel\mathcal{N}\left(x_{t-1};\mu_{\boldsymbol{\theta}},\Sigma_{q}\left(t\right)\right)) \
&=\arg\min_{\boldsymbol{\theta}}\frac{1}{2\sigma_{q}^{2}(t)}\left[\left\|\frac{1}{\sqrt{\alpha_{t}}}x_{t}-\frac{1-\alpha_{t}}{\sqrt{1-\bar{\alpha}_{t}}\sqrt{\alpha_{t}}}\hat{\epsilon}_{\theta}(x_{t},t)-\frac{1}{\sqrt{\alpha_{t}}}x_{t}+\frac{1-\alpha_{t}}{\sqrt{1-\bar{\alpha}_{t}}\sqrt{\alpha_{t}}}\epsilon_{0}\right\|_{2}^{2}\right] \
&=[\dots]=\arg\min_{\boldsymbol{\theta}}\frac{1}{2\sigma_{q}^{2}(t)}\frac{(1-\alpha_{t})^{2}}{(1-\bar{\alpha}_{t})\alpha_{t}}\left[\|\epsilon_{0}-\hat{\epsilon}_{\boldsymbol{\theta}}(x_{t},t)\|_{2}^{2}\right]
\end{aligned}$$
- The algorithm boils down to finding an approximation $\hat{\epsilon}$ of the noise (usually via the U-net VAE) that corrupted the current state $x_{t}$, $\mu_{\theta}$ is retrieved afterwards. 
- The algorithm is considered to be a <span style="color:#ff0000">finite time method</span> (fixed noise schedule and finite steps estimation).

<u>See also:</u> 
- Basics: [[Diffusion Models for Generative Artificial Intelligence An Introduction for Applied Mathematicians.pdf]]
- [[Diffwave, A Versatile Diffusion Model For Audio Synthesis.pdf]]
- [[High-Resolution Image Synthesis with Latent Diffusion Models.pdf]]
- [[Imagen Video, High Definition Video Generation With Diffusion Models.pdf]]
#### <u>Conditional diffusion approach</u>

- [[Conditional Image Generation with Score-Based Diffusion Models.pdf]]
- [[Diffusion Models Beat GANs on Image Synthesis]] supplemental: mean shift of the unconditional approach
- [[Turbulent Flow Simulation Using Autoregressive Conditional Diffusion Models.pdf]] → $\phi$
- [[Uncertainty-aware Surrogate Models for Airfoil Flow Simulations with Denoising Diffusion Probabilistic Models.pdf]] → $\phi$

#### <u>Non-Markovian diffusion (implicit models) DDIM</u> 

From [[Denoising Diffusion Implicit Models.pdf]], the idea is to leverage the knowledge of the forward process to sample from a non-Markovian backward process. Thus, one can generate new samples by using the backward process on a smaller set than the initial set of the forward degradation process. 

Those networks are also connected to the neural ODEs: $\mathrm{d}\bar{\boldsymbol{x}}(t)=\epsilon_\theta^{(t)}\left(\frac{\bar{\boldsymbol{x}}(t)}{\sqrt{\sigma^2+1}}\right)\mathrm{d}\sigma(t)$, though the sampling is different since the process is non continuous. The main advantage is the faster sampling procedure. DDIMs are basically the ODE variants of DDPMs. 

#### <u>Non Gaussian diffusion flows</u>

- [[Cold Diffusion, Inverting Arbitrary Image Transforms Without Noise.pdf]]
- State-of-the-art for arbitrary DDPM diffusion flow (cold diffusion, heat dissipation diffusion, blurring, time scaling): [[Blurring Diffusion Models.pdf]]
#### <u>Cascade diffusion models</u> 

From [[Cascaded Diffusion Models for High Fidelity Image Generation.pdf]], a low resolution generative models combined with super resolution diffusion models.

---
## 2. Score-Matching + continuous time approach (SDE) <span style="color:#ff0000">SMSDE</span> [[Score-Based Generative Modeling Through Stochastic Differential Equations.pdf]] *a.k.a. <span style="font-style:italic; color:#ff0000">score-based flows</span>:

#### <u>SDE formulation</u>

We want to evaluate the change in state with infinitesimal time step of the Markov chain of DDPM. When doing that, we obtain the Fokker-Planck equation/forward Kolmogorov equation:
$$\frac\partial{\partial t}p(x;t|y;s)=-\frac\partial{\partial x}(f(x,t)p(x;t|y;s))+\frac12\frac{\partial^2}{\partial x^2}\left(g^2(x;t)p(x;t|y;s)\right)$$
such that: $\mathbb{E}[\mathrm{d}X]=f(X,t)\mathrm{d}t \quad \text{\&} \quad \mathrm{Var}(\mathrm{d}X)=g^2(X,t)\mathrm{d}t-O(\mathrm{d}t^2)\approx g^2(X,t)\mathrm{d}t$

Thus, we recognize the drift ($\mu \sim f$) and diffusion coefficient ($D=\frac{\sigma^2}{2}$) and we can write the Îto diffusion SDE (see https://en.wikipedia.org/wiki/Fokker%E2%80%93Planck_equation): 
$$\mathrm{d}X=f(X,t)\mathrm{d}t+g(X,t)\mathrm{d}w \sim \mu(X_t,t)dt+\sigma(X_t,t)dW_t$$
Reversing time yields (https://www.vanillabug.com/posts/sde/) the reverse-time SDE:
$$\begin{aligned}
\text{dX} &=\left(\mathbf{f}(\mathbf{X},t)-\frac{g^2(t)}{p(\mathbf{X},t)}\nabla_\mathbf{X}p(\mathbf{X},t)\right)\mathrm{d}t+g(t)\mathrm{d}\mathbf{w} \\
&=\left(\mathbf{f}(\mathbf{X},t)-g^2(t)\nabla_\mathbf{X}\log p(\mathbf{X},t)\right)\mathrm{d}t+g(t)\mathrm{d}\mathbf{w} 
\end{aligned}$$
To find the values of $f$ and $g$, we look at the $\beta$ noise schedule: $\mathbf{x}_{t+\mathbf{d}t}-\mathbf{x}_t=\left(\sqrt{1-\beta(t)\mathrm{d}t}-1\right)\mathbf{x}_t+\sqrt{\beta(t)\mathrm{d}t}\epsilon(t)$ $\Rightarrow$ $\mathrm{d}\mathbf{x}=-\frac12\beta(t)\mathbf{x}\mathrm{d}t+\sqrt{\beta(t)}\mathrm{d}\mathbf{w}$  
And the reverse time Ito diffusion is obtained trivially: $\mathrm{d}\mathbf{x}=\left(-\frac12\beta(t)\mathbf{x}-\beta(t)\nabla_x\log p(\mathbf{x};t)\right)\mathrm{d}t+\sqrt{\beta(t)}\mathrm{d}\mathbf{w}$

This boils down to learning the score $\nabla_x\log p(\mathbf{x};t)$ via the <span style="color:#ff0000">Fischer divergence</span> (instead of the classical MLE objective) and use solvers (e.g. solve via the Euler-Maruyama method) to generate samples from the reverse time SDE:
$$\mathcal{L}_t(\theta)=\mathbb{E}_{p(\mathbf{x}_t,t)}\left[\|s(\mathbf{x}_t,t;\theta)-\nabla_{\mathbf{x}_t}\log p(\mathbf{x}_t;t)\|^2\right] \equiv \mathbb{E}_{t\sim\mathrm{U}[0,T]}\mathbb{E}_{p(\mathbf{x}_0;0)}\mathbb{E}_{p(\mathbf{x}_t;t|\mathbf{x}_0,0)}\left[\lambda(t)\|s(\mathbf{x}_t,t;\theta)-\nabla_{\mathbf{x}_t}\log p(\mathbf{x}_t;t|\mathbf{x}_0,0)\|^2\right]$$
The marginal is often intractable, we changed it to a conditional one. Sampling can be made via Langevin dynamics + corrected with this sampling SDE solver approach (Predictor (Langevin) - Corrector (SDE) approach). We learn the velocity field by means of learning the score function.

- The variance schedule can be changed through the SDE term. This is governed by an <span style="color:#ff0000">ODE</span> which gets us back to the first approach [[Building Normalizing Flows With Stochastic Interpolants.pdf]].
- The algorithm is considered <span style="color:#ff0000">continuous time</span> scale (although in practice, it is finite time, but the problem is continuous).
- Complexity is induced by the demultiplication of time steps.
- Score and noise does not seem to play a huge role in the performances. 

The specific choice of the noise schedule has been generalized to Ornstein-Uhlenbeck (OU) process forms: $dr_t=-\theta(r_t-\mu)dt+\sigma dW_t$.

#### <u>Conditional approach</u>

-to do-
#### <u>ODE Formulation</u> [[Elucidating the Design Space of Diffusion-Based Generative Models.pdf]]

A remarkable property of this SDE is the existence of an ordinary differential equation (ODE), dubbed the probability flow (PF) ODE (<span style="color:#ff0000">probability flow equation</span>, [[Score-Based Generative Modeling Through Stochastic Differential Equations.pdf]]) whose solution trajectories sampled at $t$ are distributed according to $p_t(\mathbf{x})$. Nullifying the noise injection in the SDEs yields the so-called Probability Flow ODE, that simply describe the deterministic trajectory of a noisy sample projected back onto the true density. These ODEs provide a smooth, deterministic mapping between the Gaussian noise density and the true density.
$$
SDE \iff \mathrm{d}\mathbf{x}_{t}=\left[\boldsymbol{\mu}(\mathbf{x}_{t},t)-\frac{1}{2}\sigma(t)^{2}\nabla\log p_{t}(\mathbf{x}_{t})\right]\mathrm{d}t
$$
Consistency models, a direct application [[Consistency Models.pdf]]. Backwards solvers are used to sample the original data distribution.
The most important thing to notice is that this approach is deterministic.

#### <u>Deterministic approach</u>

Several non-stochastic methods exist for diffusion. One of them, that requires fewer knowledge of statistics and provide greater training and sampling stability, is [[Iterative α-(de)Blending, a Minimalist Deterministic Diffusion Model.pdf]] (find an invertible function to go from one density to another). The ODE formulation is also deterministic.

<span style="color:#ff0000">Deterministic trajectories == flow based models</span>

---
## 3. Langevin dynamics and score-matching <span style="color:#ff0000">SMLD</span> [[Improved Techniques for Training Score-Based Generative Models.pdf]]:

The sampling of the data distribution is made possible via the estimation of the score (skipping an estimation of $p_{data}$): $\frac12\mathbb{E}_{p_{\mathrm{data}}}[\|\mathbf{s_{\theta}}(\mathbf{x})-\nabla_{\mathbf{x}}\log p_{\mathrm{data}}(\mathbf{x})\|_{2}^{2}]$. The sampling is then made using Langevin dynamics:
$$\tilde{\mathbf{x}}_t=\tilde{\mathbf{x}}_{t-1}+\frac{\epsilon}{2}\nabla_{\mathbf{x}}\log p(\tilde{\mathbf{x}}_{t-1})+\sqrt{\epsilon}\mathbf{z}_t \quad \text{such that }\mathbf{z}_{t}\sim\mathcal{N}(0,I)$$
This is not diffusion but matches the score-matching approach of SMSDE and is a precursor to generative modeling.

<u>NB</u>:  SMLD, DDPM and SMSDE are unified with SMSDE under a SDE problem formulation.

<u>See also</u>:
- [[Generative Modeling by Estimating Gradients of the Data Distribution.pdf]]
- [[Deep Unsupervised Learning using Nonequilibrium Thermodynamics.pdf]]

<u>Score matching state-of-the-art</u>:
- [[Sliced Score Matching A Scalable Approach to Density and Score Estimation.pdf]]
- [[A Connection Between Score Matching and Denoising Autoencoders.pdf]]
- [[Interpretation and Generalization of Score Matching.pdf]]
#### <u>Other score matching approaches</u>

- [[Bayesian Learning via Stochastic Gradient Langevin Dynamics.pdf]]
- [[Estimation of Non-Normalized Statistical Models by Score Matching.pdf]]

---
## 4. Bridge based methods [[Simulating Diffusion Bridges with Score Matching.pdf]]

-to do-
[[Diffusion Schrödinger Bridge with Applications to Score-Based Generative Modeling.pdf]]

<u>Force field approaches</u>:
- [[Two for One; Diffusion Models and Force Fields for Coarse-Grained Molecular Dynamics.pdf]]
- [[Interpretable ODE-style Generative Diffusion Model via Force Field.pdf]] via $\phi$

<u>See also</u>:
- [[Diffusion-based Molecule Generation with Informative Prior Bridges.pdf]]
- $\phi$ informed diffusion: [[FusionDock; Physics-informed Diffusion Model for Molecular Docking.pdf]]

---
## 5. “Arbitrary” flow ($\neq$ probability POV) diffusion [[Stochastic Interpolants, A Unifying Framework for Flows and Diffusions.pdf]]

Be able to go from $p_{0}$ to $p_{1}$ and vis-versa via a stochastic interpolent [[Building Normalizing Flows With Stochastic Interpolants.pdf]]. 
The idea is that there exist an optimal transport between the two densities via interpolants: [[Rectified Flow; A Marginal Preserving Approach to Optimal transport.pdf]]. Seemingly, ODE based methods are both strong and traceable methods as long as the score is learned sufficiently well.
We want to sample from one distribution and obtain a sample from the other distribution via the built interpolant link.

Related work [[Stochastic Interpolants, A Unifying Framework for Flows and Diffusions.pdf#page=5&selection=79,1,80,12|Stochastic Interpolants, A Unifying Framework for Flows and Diffusions, page 5]]:
- Deterministic Transport and Normalizing Flows
- Stochastic Transport and Score-Based Diffusions (SBDMs)
- Stochastic Interpolants, Rectified Flows, and Flow matching
- Optimal Transport and Schrödinger Bridges
- Convergence bounds

A stochastic interpolant is assumed to be under the form: $x_t=I(t,x_0,x_1)+\gamma(t)z$ with $z\sim \mathcal{N}(0,Id)$ and $I(0,x_0,x_1)=x_0$, $I(1,x_0,x_1)=x_1$ and $t \in[0,1]$.

This technique resemble other “degradation”/”restoration” diffusion methods that degrades and image via the degradation operator $D$ and then sample a new one via the restoration operator $R_{\theta}$. It is built with the knowledge of the previously constructed degradation operator. In other words, learn $\theta$ ML via: $\min_{\theta}\mathbb{E}_{x\sim\mathcal{X}}\|R_{\theta}(D(x,t),t)-x\|$.

This idea is pushed in many meaningful different ways, associating SDE-diffusion/DDPM to any physical meaning (theoretically bidirectional in time):

- Consider the physical system as a SDE [[Solving Inverse Physics Problems with Score Matching.pdf]]:
$$d\mathbf{x}=\mathcal{P}(\mathbf{x})dt+g(t)dW$$
Or as its ODE: $d\mathbf{x}=\left[\mathcal{P}(\mathbf{x})-\frac{1}{2}g(t)^2\nabla_\mathbf{x}\log p_t(\mathbf{x})\right]dt$. A reverse simulation of a process is obtained by score-matching correction of the reverse simulator, $\mathbf{x}_m\approx\mathbf{x}_{m+1}+\Delta t\left[\tilde{\mathcal{P}}^{-1}(\mathbf{x}_{m+1})+s_\theta(\mathbf{x}_{m+1},t_{m+1})\right]$ which yields a reverse SDE: $d\mathbf{x}=\left[-\tilde{\mathcal{P}}^{-1}(\mathbf{x})-2s_{\theta}(\mathbf{x},t)\right]dt+g(t)\boldsymbol{d}W$ such that $\mathcal{P}=-\tilde{\mathcal{P}}$.

- See the diffusion steps as an actual simulation unfolding [[DYffusion; A Dynamics-informed Diffusion Model for Spatiotemporal Forecasting.pdf]].

A  “degradation”/”restoration” technique but the degradation is learned to imitate the simulation unfolding and the restoration acts like a forecaster. Similar to cold diffusion.

<u>See also</u>:
- [[Soft Diffusion Score Matching For General Corruptions.pdf]]

---
## 6. Poisson flow generative models <span style="color:#ff0000">PFGM</span> [[Poisson Flow Generative Models.pdf]]:

Maps an arbitrary distribution into a N-dimensionnal hemisphere via Poisson’s law. The reverse method also comes from electric processes ODEs. Once inside the hemisphere, perturbation is added, we thus obtain several training points.
The backward sampling trajectory is achieved via the following backwards ODE:
$$d(\mathbf{x},z)=\bigl(\frac{d\mathbf{x}}{dt}\frac{dt}{dz}dz,dz\bigr)=\bigl(\mathbf{v}(\tilde{\mathbf{x}})_{\mathbf{x}}\mathbf{v}(\tilde{\mathbf{x}})_{z}^{-1},1\bigr)dz$$
where basically $\mathbf{v}$ is the normalized filed obtained via solving the field of the Poisson flow equation and then learn the normalized field. $z$ is an extension of the $x$ plan to avoid collapsing on the center. The novelty resides in building the ODE from a physical PDE + no score-matching is involved (analytical solver thanks to Green).

<u>NB</u>: Apparently achieves faster sampling than SDE solvers and better generation performances.

<u>See also</u>:
- Improved version [[PFGM++; Unlocking the Potential of Physics-Inspired Generative Models.pdf]], apparently, $\phi$ inspired, a lesser version of GenPhys.

---
## 7.   GenPhys [[Genphys, From Physical Processes To Generative Models.pdf]]:

Construct a generative model from a physical process. This is achieved by converting the PDE into a s-generative PDE.
Recall the probability  flow equation: $\frac{\mathrm{d}\mathbf{x}}{\mathrm{dt}}=\tilde{f}(\mathbf{x},t)$ from [[From FPK to SDE-Var to ODE]], we can evolve the process forward and backward to generate samples from two distributions. This physical process can evolve the probability distribution $p(\mathbf{x}, t)$ as a more general form (with a reminder/non conservative term $R$), also know as the probability flow equation (<span style="color:#ff0000">just like the ODE formulation</span>): 
$$\dfrac{\partial p(\mathbf{x},t)}{\partial t}+\nabla\cdot[p(\mathbf{x},t)\mathbf{v}(\mathbf{x},t)]-R(\mathbf{x},t)=0$$
One can rewrite some PDE systems (there are constrains on the system for the method to work) under that from and then solve $\dfrac{\partial p(\mathbf{x},t)}{\partial t}+\nabla\cdot[p(\mathbf{x},t)\mathbf{v}(\mathbf{x},t)]-R(\mathbf{x},t)=PDE=p_{data}(\mathbf{x})\delta(t)$. The sampling is obtained almost similarly as an ODE method, the relation $\frac{d\mathbf{x}(t)}{dt}=\mathbf{s}_{\theta}(\mathbf{x},t)$ holds true. It resembles the Poisson flow generative models but expands it for more general PDE and base its sampling from the score-matching method.

---
## 8. ML approaches for $\Phi$ problems

- [[Learning to Control PDEs with Differentiable Physics.pdf]]
- [[Deep Fluids; A Generative Network for Parameterized Fluid Simulations.pdf]]

---

#research-clue 

[[Interpretable ODE-style Generative Diffusion Model via Force Field.pdf]]






